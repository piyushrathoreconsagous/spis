
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://ticket.consagous.co.in/admin/template/js/parsley.js"></script>

<link href="https://ticket.consagous.co.in/admin/template/css/parsley.css" rel="stylesheet" type="text/css" />
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<br><br><br><br>
<div class="container">
    <div class="response-msg">

    </div>
        
        
<form class="demo-form" id="contact-form" method="post">
    @csrf
  <div class="form-section">
    <label for="firstname">First Name:</label>
    <input type="text" class="form-control" name="firstname" required="" data-parsley-required-message="First name is required">

    <label for="lastname">Last Name:</label>
    <input type="text" class="form-control" name="lastname" required="" data-parsley-required-message="Last name is required">
  
    <label for="email">Email:</label>
    <input type="email" class="form-control" name="email" required="" data-parsley-required-message="Email is required">

    <label for="lastname">Contact:</label>
    <input type="text" class="form-control" name="contact" required="" data-parsley-required-message="Contact is required" data-parsley-type="digits" data-parsley-type-message="only numbers are allowed" data-parsley-minlength="10" data-parsley-maxlength="12">  
  </div>

  <div class="form-section">
    <label for="color">date of birth:</label>
    <input type="date" class="form-control" name="dob" id="datepicker" required="" data-parsley-required-message="Dob is required">

    <label for="color">salary:</label>
    <input type="text" class="form-control" name="salary" required="" data-parsley-required-message="Salary is required">
  </div>

  <div class="form-navigation">
    <button type="button" class="previous btn btn-info pull-left">&lt; Previous</button>
    <button type="button" class="next btn btn-info pull-right">Next &gt;</button>
    <input type="submit" class="btn btn-default pull-right">
    <span class="clearfix"></span>
  </div>

</form>
</div>


<script type="text/javascript">
$(function () {
  var $sections = $('.form-section');

  function navigateTo(index) {
    // Mark the current section with the class 'current'
    $sections
      .removeClass('current')
      .eq(index)
        .addClass('current');
    // Show only the navigation buttons that make sense for the current section:
    $('.form-navigation .previous').toggle(index > 0);
    var atTheEnd = index >= $sections.length - 1;
    $('.form-navigation .next').toggle(!atTheEnd);
    $('.form-navigation [type=submit]').toggle(atTheEnd);
  }

  function curIndex() {
    // Return the current index by looking at which section has the class 'current'
    return $sections.index($sections.filter('.current'));
  }

  // Previous button is easy, just go back
  $('.form-navigation .previous').click(function() {
    navigateTo(curIndex() - 1); 
  });

  // Next button goes forward iff current block validates
  $('.form-navigation .next').click(function() {
    $('.demo-form').parsley().whenValidate({
      group: 'block-' + curIndex()
    }).done(function() {
      navigateTo(curIndex() + 1);
    });
  });

  // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
  $sections.each(function(index, section) {
    $(section).find(':input').attr('data-parsley-group', 'block-' + index);

  });
  navigateTo(0); // Start at the beginning
});


    $(function(){
        $("#contact-form").submit(function(e){
               e.preventDefault();
            var form = $(this);

            form.parsley().validate();

            if (form.parsley().isValid()){
                contactus();
            }
        });
    });


 
function contactus()
{
    $.ajax({
           type: "POST",
           url: "{{ url('contactus') }}",
           data:  $('#contact-form').serialize(), // serializes the form's elements.
           dataType: "json",
           success: function(data)
           {
                if(data.success==1)
                {
                    $(".response-msg").html('<div class="alert alert-success" style="text-align: center; height: 50px;"><p>'+data.message+'</p></div>');     
                    $("#contact-form")[0].reset();  $(".previous").click(); 
                }
                else if(data.success==0)
                {
                    $(".response-msg").html('<div class="alert alert-danger" style="text-align: center; height: 50px;"><p>'+data.message+'</p></div>');
                }               
                $('.alert').show().delay(5000).fadeOut();
           }
         });
}
    
</script>

<style type="text/css">
    .form-section {
  padding-left: 15px;
  border-left: 2px solid #FF851B;
  display: none;
}
.form-section.current {
  display: inherit;
}
.btn-info, .btn-default {
  margin-top: 10px;
}
html.codepen body {
  margin: 1em;
}

</style>