<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contactus extends Model
{
    use SoftDeletes;

    public $table = 'lara_contactus';

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'contact',
        'dob',
        'salary',
        'created_dt',
        'updated_at',
    ];

}
