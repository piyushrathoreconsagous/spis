<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Validator;  // load the validation library
use App\Contactus;
use Session;

class HomeController extends Controller
{
    public function contactus()
    {
    	return view('contact-us');
    }

    public function store_contactus(Request $request)
    {
    	$validator = Validator::make($request->input(),
			array('firstname'	 => 'required',
				  'lastname'	 => 'required',
				  'email' 		 => 'required|email',
				  'contact'		 => 'required',
				  'dob'			 => 'required',
				  'salary'       => 'required',
				));
		
		if ($validator->fails())
		{
			$result = $validator->messages()->first();
			return back()->with('error', $result);        
		}
		else
		{	
			$contactus = new Contactus();
		    $contactus->first_name  = $request->input('firstname');
		    $contactus->last_name 	= $request->input('lastname');
		    $contactus->email  	    = $request->input('email');
		    $contactus->contact  	= $request->input('contact');
		    $contactus->dob  		= $request->input('dob');
		    $contactus->salary  	= $request->input('salary');
		    $contactus->created_at  = date("Y-m-d");
	 	    $contactus->save();

			$response = array("success"=>1, "message"=>"Contactus Successfully");
			echo json_encode($response);
		}

    }

}
