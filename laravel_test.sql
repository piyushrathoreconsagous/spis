-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2020 at 02:42 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `lara_contactus`
--

CREATE TABLE `lara_contactus` (
  `id` int(11) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `dob` date NOT NULL,
  `salary` varchar(50) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lara_contactus`
--

INSERT INTO `lara_contactus` (`id`, `first_name`, `last_name`, `email`, `contact`, `dob`, `salary`, `created_at`, `updated_at`) VALUES
(14, 'Mahavir', 'Pawecha', 'aashish421@gmail.com', '87987464564', '2020-05-21', '2000', '2020-05-30', '2020-05-30'),
(15, 'Mahavir', 'Pawecha', 'aashish421@gmail.com', '87987464564', '2020-05-21', '2000', '2020-05-30', '2020-05-30'),
(16, 'Mahavir', 'Pawecha', 'aashish421@gmail.com', '87987464564', '2020-05-21', '2000', '2020-05-30', '2020-05-30'),
(17, 'Mahavir', 'rathore', 'aashish421@gmail.com', '87987464564', '2020-05-13', '2000', '2020-05-30', '2020-05-30'),
(18, 'Mahavir', 'Pawecha', 'naman@gmail.com', '87987464564', '2020-05-14', '2000', '2020-05-30', '2020-05-30'),
(19, 'Mahavir', 'Pawecha', 'piyush.rathore@consagous.com', '87987464564', '2020-05-21', '2000', '2020-05-30', '2020-05-30'),
(20, 'Mahavir', 'rathore', 'piyush.rathore@consagous.com', '87987464564', '2020-04-29', 'gsgfds', '2020-05-30', '2020-05-30'),
(21, 'Mahavir', 'Pawecha', 'piyush.rathore@consagous.com', '87987464564', '2020-05-12', '2000', '2020-05-30', '2020-05-30'),
(22, 'Mahavir', 'rathore', 'piyush.rathore@consagous.com', '87987464564', '2020-05-12', '2000', '2020-05-30', '2020-05-30'),
(23, 'Mahavir', 'Pawecha', 'piyush.rathore@consagous.com', '87987464564', '2020-05-13', '2000', '2020-05-30', '2020-05-30'),
(24, 'Mahavir', 'rathore', 'piyush.rathore@consagous.com', '87987464564', '2020-05-05', '2000', '2020-05-30', '2020-05-30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lara_contactus`
--
ALTER TABLE `lara_contactus`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lara_contactus`
--
ALTER TABLE `lara_contactus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
